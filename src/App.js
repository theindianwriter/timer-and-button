import React from 'react';

import './App.css';
import Button from './components/Button.js';
import Timer from './components/Timer.js';




class App extends React.Component{
   state={status : {1 : false,2 : false} };
   buttonClicked = (id) =>{
      this.setState(prevState=>{
         let prevStateStatus = this.state.status[id];
         return ({status:{...prevState.status, [id]: !prevStateStatus}});
       });
   }
  
  
   checkOn = () => { 
     if(this.state.status[1] && this.state.status[2])
        return true;
     else return false;
   }
  
   updateAll(mainStatus){
      this.setState(()=>{
         if(mainStatus)
             return ({status :{1: true,2: true}});
         return ({status :{1: false,2: false}});
      });
   }
  
   render(){
      return(
       <div className="App"> 
            <Timer status={this.state.status[1]} name="1"></Timer>
            <Button status={this.state.status[1]} handleClicked={() => this.buttonClicked(1)} />
            <Timer status={this.state.status[2]} name="2"></Timer>
            <Button status={this.state.status[2]} handleClicked={() => this.buttonClicked(2)} />
            <br></br><br></br>
            <Button status={this.checkOn()} handleClicked = {() => this.updateAll( !this.checkOn())} />
        </div>
      );
  
   }
  
}

export default App;

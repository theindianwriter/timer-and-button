import React from 'react';

import './buttonStyle.css';

class Button extends React.Component{
    render(){
      
      return(
        <button onClick={this.props.handleClicked}>{this.props.status ? 'ON':'OFF'}
        </button>
        );
    }
  }

export default Button;

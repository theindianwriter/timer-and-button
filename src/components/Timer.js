import React from 'react';



class Timer extends React.Component{
    state={ min: 0, sec: 0
    };
    
    componentDidUpdate(prevProps){
      if(prevProps.status !== this.props.status)
        this.check();
    }

    componentWillUnmount(){
      clearInterval(this.timerId);
    }
    
    check=()=>{
      if(this.props.status)
        this.startTimer();
      else{ 
        this.stopTimer();
      }
    }
    startTimer=() => {this.timerId = setInterval(()=>{
        this.setState((prevState)=>{
          let prevStateMin = this.state.min;
          let prevStateSec = this.state.sec;
          if((++prevStateSec) === 60){
            prevStateMin++;
            prevStateSec = 0
           }
          return ({min: prevStateMin,sec:prevStateSec });})
        },1000);
    }
    
    stopTimer = ()=>{
       clearInterval(this.timerId);
    }
    
    
    render(){
    
      return(
      <div>
           <h1>the timer {this.props.name}</h1>
           <h2>{(this.state.min < 10) ? ("0"+this.state.min):(this.state.min)} : {(this.state.sec < 10) ? ("0"+this.state.sec):(this.state.sec)}</h2>
      </div>);
    }
    }


    export default Timer;